const router = require("express").Router();
const Post = require("../models/Post");
const User = require("../models/User");

// Create a post

router.post("/create", async (req, res) => {
  const newPost = new Post(req.body);

  try {
    const savedPost = await newPost.save();
    res
      .status(200)
      .json({ message: "Post saved Successfully", post: savedPost });
  } catch (error) {
    res.status(500).json({ message: "Internal server error", error: error });
  }
});

// Update a post

router.put("/:id", async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (post.userId === req.body.userId) {
      await post.updateOne({ $set: req.body });
      res.status(200).json({ message: "Post Saved Successfully", post: post });
    } else {
      res.status(403).json({ message: "You can update only your post" });
    }
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error", error: error });
  }
});

// delete a post

router.delete("/:id", async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (req.body.userId === post.userId) {
      await post.deleteOne();
      res.status(200).json({ message: "Post deleted successfully" });
    } else {
      res.status(403).json({ message: "You can delete only your post" });
    }
  } catch (error) {
    res.status(500).json({ message: "Internal server error", error: error });
  }
});

// like a post

router.put("/:id/like", async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (req.body.userId !== post.userId) {
      if (!post.likes.includes(req.body.userId)) {
        await post.updateOne({ $push: { likes: req.body.userId } });
        res.status(200).json({ message: "You liked this post", post: post });
      } else {
        await post.updateOne({ $pull: { likes: req.body.userId } });
        res.status(200).json({ message: "You disliked this post", post: post });
      }
    } else {
      res.status(403).json({ message: "You cannot Like your own post" });
    }
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error", error: error });
  }
});
// Get a post

router.get("/:id", async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    res.status(200).json(post);
  } catch (error) {
    res.status(500).json({ message: "Internal server error" });
  }
});

// Get timeline posts

router.get("/timeline/all", async (req, res) => {
  try {
    const currentUser = await User.findById(req.body.userId);
    const userPosts = await Post.find({ userId: currentUser._id });

    const friendPosts = await Promise.all(
      currentUser.following.map((friendId) => {
        Post.find({ userId: friendId });
      })
    );
    res.status(200).json(userPosts.concat(...friendPosts));
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
      error: error,
    });
  }
});

module.exports = router;
