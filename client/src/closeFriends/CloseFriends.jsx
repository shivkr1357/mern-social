import "./closeFriends.css";

const CloseFriends = ({ f }) => {
  return (
    <li className="sidebarFriend">
      <img src={f.profilePicture} className="sidebarFriendImg" alt="img" />
      <span className="sidebarFriendName">{f.username}</span>
    </li>
  );
};

export default CloseFriends;
