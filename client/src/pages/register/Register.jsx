import "./register.css";

const Register = () => {
  return (
    <div className="register">
      <div className="registerWrapper">
        <div className="registerLeft">
          <div className="registerLogo">Social Media</div>
          <span className="registerDesc">
            Connect with friends around the world and around You
          </span>
        </div>
        <div className="registerRight">
          <div className="registerBox">
            <input
              type="text"
              placeholder="Username"
              className="registerInput"
            />
            <input type="text" placeholder="Email" className="registerInput" />
            <input
              type="text"
              placeholder="Password"
              className="registerInput"
            />
            <input
              type="password"
              placeholder="Password Again"
              className="registerInput"
            />
            <button className="registerButton">Sign Up</button>
            <span className="registerForget">Forget Password</span>
            <button className="registerRegisterButton">Log into Account</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
